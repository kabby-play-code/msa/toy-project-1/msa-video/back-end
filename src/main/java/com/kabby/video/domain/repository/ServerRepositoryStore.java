package com.kabby.video.domain.repository;

import java.util.List;

import com.kabby.video.domain.model.Server;

public interface ServerRepositoryStore {
	
	public void createServer(Server server);
	public List<Server> retrieveAllServer();
	public Server retrieveServerByServerIp(String serverIp);

}
