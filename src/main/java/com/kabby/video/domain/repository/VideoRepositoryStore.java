package com.kabby.video.domain.repository;

import java.util.List;

import com.kabby.video.domain.model.Video;

public interface VideoRepositoryStore {

	public void createVideo(Video video);
	public List<Video> retrieveAllVideo();
	public Video retrieveVideoById(String videoId);
}
