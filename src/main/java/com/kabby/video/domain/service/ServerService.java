package com.kabby.video.domain.service;

import java.util.List;

import com.kabby.video.domain.model.Server;

public interface ServerService {
	public String registerServer(Server server);
	public List<Server> findAllServer();
	public Server findByServerIp(String serverIp);

}
