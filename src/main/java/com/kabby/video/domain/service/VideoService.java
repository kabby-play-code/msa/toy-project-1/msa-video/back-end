package com.kabby.video.domain.service;

import java.io.FileNotFoundException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.kabby.video.domain.model.Video;

public interface VideoService {
	public String registerVideo(Video video);
	public List<Video> findAllVideo();
	public Video findVideo(String videoId);
	public StreamingResponseBody findVideoFileDownload(String videoId, HttpServletResponse response) throws FileNotFoundException;
	
	
}
