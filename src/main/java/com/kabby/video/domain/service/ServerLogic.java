package com.kabby.video.domain.service;

import java.util.ArrayList;
import java.util.List;

import com.kabby.video.domain.model.Server;
import com.kabby.video.domain.model.Video;
import com.kabby.video.domain.repository.ServerRepositoryStore;

public class ServerLogic implements ServerService {

	private ServerRepositoryStore serverRepositoryStore;

	public ServerLogic(ServerRepositoryStore serverRepositoryStore) {
		this.serverRepositoryStore = serverRepositoryStore;
	}

	@Override
	public String registerServer(Server server) {
		// TODO Auto-generated method stub

		List<Video> videos = new ArrayList<>();

		for (Video video : server.getVideos()) {

			video.setVideoServerIp(server.getServerIp().toString());
			videos.add(video);
		}

		server.setVideos(videos);
		this.serverRepositoryStore.createServer(server);
		return server.getServerIp();
	}

	@Override
	public List<Server> findAllServer() {
		// TODO Auto-generated method stub
		return this.serverRepositoryStore.retrieveAllServer();
	}

	@Override
	public Server findByServerIp(String serverIp) {
		// TODO Auto-generated method stub
		return this.serverRepositoryStore.retrieveServerByServerIp(serverIp);
	}

}
