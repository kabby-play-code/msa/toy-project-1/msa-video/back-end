package com.kabby.video.domain.service;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.kabby.video.domain.model.Video;
import com.kabby.video.domain.repository.VideoRepositoryStore;

public class VideoLogic implements VideoService {

	private VideoRepositoryStore videoRepository;

	public VideoLogic(VideoRepositoryStore videoRepository) {
		this.videoRepository = videoRepository;
	}

	@Override
	public String registerVideo(Video video) {
		// TODO Auto-generated method stub
		this.videoRepository.createVideo(video);
		return video.getVideoId();
	}

	@Override
	public List<Video> findAllVideo() {
		// TODO Auto-generated method stub
		return this.videoRepository.retrieveAllVideo();
	}

	@Override
	public Video findVideo(String videoId) {
		// TODO Auto-generated method stub
		return this.videoRepository.retrieveVideoById(videoId);
	}

	@SuppressWarnings("resource")
	@Override
	public StreamingResponseBody findVideoFileDownload(String videoId, HttpServletResponse response) throws FileNotFoundException {
		// TODO Auto-generated method stub
		
		/*
		 * 경로 : /Users/jeong/Desktop/video/tea.mp4
		 * 확장자 : mp4
		 * 
		 */

		Video video=this.videoRepository.retrieveVideoById(videoId);
						
		response.setContentType("video/mp4");
		response.setHeader("Content-Disposition", "attachment;filename=" + video.getVideoName() + "." + video.getVideoExtention());
		// JSch jsch = new JSch();

		File targetFile = new File(video.getVideoPath());
		InputStream targetStream =  new DataInputStream(new FileInputStream(targetFile));
		
		return outputStream -> {
			int nRead;
			byte[] data = new byte[4069];
			while ((nRead = targetStream.read(data, 0, data.length)) != -1) {
				outputStream.write(data, 0, nRead);
			}
			
		};
	}

}
