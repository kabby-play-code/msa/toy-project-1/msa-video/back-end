package com.kabby.video.domain.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Server {


	private String serverIp;
	private String userName;
	private String userPassword;
	
	private List<Video> videos;	
	
	public void addVideo(Video video) {
		this.videos.add(video);
	}
	
	public Server() {
		
	}
	
	
}
