package com.kabby.video.domain.model;

import java.time.LocalDateTime;
import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Video {

	private String videoId;			         //영상 고유아이디
	private String videoName;   	         //영상 이름
	private String videoExtention;           //영상 확장자
	private String videoPath; 		         //영상 저장 경로
	private LocalDateTime videoCreatedDate;  //영상 업로드 일자
	
	
	private String videoServerIp;            //영상 서버 아이피
	
	

	
    public Video() {
        //
        this.videoId = UUID.randomUUID().toString();
    }
	
	
}
