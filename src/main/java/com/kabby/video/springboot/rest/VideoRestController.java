package com.kabby.video.springboot.rest;

import java.io.FileNotFoundException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.kabby.video.domain.model.Video;
import com.kabby.video.domain.service.VideoService;
import com.kabby.video.springboot.service.VideoServiceImpl;

@RestController
@RequestMapping(value="video")
public class VideoRestController implements VideoService{

	private VideoServiceImpl videoServiceImpl;
	
	public VideoRestController(VideoServiceImpl videoServiceImpl) {
		this.videoServiceImpl=videoServiceImpl;
	}

	@Override
	@PostMapping(value = {"", "/"})
	public String registerVideo(@RequestBody Video video) {
		// TODO Auto-generated method stub
		
		return this.videoServiceImpl.registerVideo(video);
	}

	@Override
	@GetMapping(value = {"", "/"})
	public List<Video> findAllVideo() {
		// TODO Auto-generated method stub
		return this.videoServiceImpl.findAllVideo();
	}

	@Override
	@GetMapping(value = "{videoId}")
	public Video findVideo(@PathVariable(name="videoId") String videoId) {
		// TODO Auto-generated method stub
		return this.videoServiceImpl.findVideo(videoId);
	}

	@Override
	@GetMapping(value = "/{videoId}/download", produces = MediaType.APPLICATION_JSON_VALUE)
	public StreamingResponseBody findVideoFileDownload(@PathVariable(name="videoId") String videoId,HttpServletResponse response) throws FileNotFoundException {
		// TODO Auto-generated method stub
		return this.videoServiceImpl.findVideoFileDownload(videoId, response);
	}
	

	
}
