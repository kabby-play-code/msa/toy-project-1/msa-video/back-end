package com.kabby.video.springboot.rest;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kabby.video.domain.model.Server;
import com.kabby.video.domain.service.ServerService;
import com.kabby.video.springboot.service.ServerServiceImpl;

@RestController
@RequestMapping(value="server")
public class ServerRestController implements ServerService {
	
	private ServerServiceImpl serverServiceImpl;
	
	public ServerRestController(ServerServiceImpl serverServiceImpl) {
		this.serverServiceImpl=serverServiceImpl;
	}

	@Override
	@PostMapping(value = {"", "/"})
	public String registerServer(@RequestBody Server server) {
		// TODO Auto-generated method stub
		return this.serverServiceImpl.registerServer(server);
	}

	@Override
	@GetMapping(value = {"", "/"})
	public List<Server> findAllServer() {
		// TODO Auto-generated method stub
		return this.serverServiceImpl.findAllServer();
	}

	@Override
	@GetMapping(value = "{serverIp}")
	public Server findByServerIp(@PathVariable(name="serverIp") String serverIp) {
		// TODO Auto-generated method stub
		return this.serverServiceImpl.findByServerIp(serverIp);
	}

}
