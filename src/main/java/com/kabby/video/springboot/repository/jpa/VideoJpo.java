package com.kabby.video.springboot.repository.jpa;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.beans.BeanUtils;

import com.kabby.video.domain.model.Video;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "TB_VIDEO")
public class VideoJpo{

	@Id
	@Column(name = "VIDEO_ID")
	private String videoId;

	
	@Column(name = "VIDEO_NAME")
	private String videoName; //비디오 이름
	
	@Column(name = "VIDEO_EXTENTION")
	private String videoExtention; //비디오 확장자
	
	@Column(name = "VIDEO_PATH")
	private String videoPath; //비디오 저장 경로
	
	@CreationTimestamp
	@Column(name = "VIDEO_CREATED_DATE")
	private LocalDateTime videoCreatedDate; //비디오 업로드 일자

	@Column(name="VIDEO_SERVER_IP")
	private String videoServerIp;

	
	public VideoJpo() {
		
	}
	
	
	
	
	public VideoJpo(Video video) {
		
		BeanUtils.copyProperties(video, this);
		
	}
	
	public Video toModel() {
	
		
		Video video= new Video();
		BeanUtils.copyProperties(this, video);
		return video;
	}
	
}
