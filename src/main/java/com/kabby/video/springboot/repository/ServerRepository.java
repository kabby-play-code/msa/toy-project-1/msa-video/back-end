package com.kabby.video.springboot.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.kabby.video.domain.model.Server;
import com.kabby.video.domain.repository.ServerRepositoryStore;
import com.kabby.video.springboot.repository.jpa.ServerJpaRepository;
import com.kabby.video.springboot.repository.jpa.ServerJpo;
import com.kabby.video.springboot.repository.jpa.VideoJpo;

@Repository
public class ServerRepository  implements ServerRepositoryStore{

	
	private ServerJpaRepository serverJpaRepository;
	
	
	public ServerRepository(ServerJpaRepository serverJpaRepository) {
		this.serverJpaRepository=serverJpaRepository;
	}
	
	@Override
	public void createServer(Server server) {
		// TODO Auto-generated method stub
		
		this.serverJpaRepository.save(new ServerJpo(server));
	}

	@Override
	public List<Server> retrieveAllServer() {
		// TODO Auto-generated method stub
		List<ServerJpo> serverJpos = this.serverJpaRepository.findAll();
		List<Server> servers=new ArrayList<Server>();
		for(ServerJpo serverJpo: serverJpos) {
			servers.add(serverJpo.toModel());
		}
		
		return servers;
	}

	@Override
	public Server retrieveServerByServerIp(String serverIp) {
		// TODO Auto-generated method stub
		Optional<ServerJpo> serverJpo=this.serverJpaRepository.findByServerIp(serverIp);
		return serverJpo.get().toModel();
	}

}
