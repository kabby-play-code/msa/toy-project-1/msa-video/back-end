package com.kabby.video.springboot.repository.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServerJpaRepository extends  JpaRepository<ServerJpo, Long>{

	public Optional<ServerJpo> findByServerIp(String serverIp);
}
