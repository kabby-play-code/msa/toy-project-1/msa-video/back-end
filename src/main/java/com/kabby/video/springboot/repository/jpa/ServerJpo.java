package com.kabby.video.springboot.repository.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.kabby.video.domain.model.Server;
import com.kabby.video.domain.model.Video;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "TB_SERVER")
public class ServerJpo {

	
	/*
	 * ServerJpo 와 VideoJpo 는 1:N 관계 (단방향)
	 * 
	 */


	@Id
	@Column(name = "SERVER_IP")
	private String serverIp;

	@Column(name = "USER_NAME")
	private String userName;

	@Column(name = "USER_PASSWORD")
	private String userPassword;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="VIDEO_SERVER_IP")
	private List<VideoJpo> videoJpos;
	
	public ServerJpo() {
		this.videoJpos = new ArrayList<VideoJpo>();
	}

	public ServerJpo(Server server) {
		
		this.videoJpos = new ArrayList<VideoJpo>();
		for(Video video : server.getVideos()) {
			video.setVideoServerIp(server.getServerIp());
			videoJpos.add(new VideoJpo(video));
			
		}
		
		
		BeanUtils.copyProperties(server, this);
		
		
		
		
		

	}

	public Server toModel() {

		Server server = new Server();
		
		
		List<Video> videos = new ArrayList<>();
		for(VideoJpo videoJpo: this.videoJpos) {
			videos.add(videoJpo.toModel());
		}
		server.setVideos(videos);
		
		BeanUtils.copyProperties(this, server);
		
		return server;
	}

}
