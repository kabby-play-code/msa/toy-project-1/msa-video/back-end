package com.kabby.video.springboot.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.kabby.video.domain.model.Video;
import com.kabby.video.domain.repository.VideoRepositoryStore;
import com.kabby.video.springboot.repository.jpa.ServerJpaRepository;
import com.kabby.video.springboot.repository.jpa.VideoJpaRepository;
import com.kabby.video.springboot.repository.jpa.VideoJpo;


@Repository
public class VideoRepository implements VideoRepositoryStore {
	
	private VideoJpaRepository videoJpaRepository;

	public VideoRepository(VideoJpaRepository videoJpaRepository) {
		this.videoJpaRepository=videoJpaRepository;
		
	}
	

	@Override
	public void createVideo(Video video) {
		// TODO Auto-generated method stub
		this.videoJpaRepository.save(new VideoJpo(video));
		
	}

	@Override
	public List<Video> retrieveAllVideo() {
		// TODO Auto-generated method stub
		List<VideoJpo> videoJpos = this.videoJpaRepository.findAll();
		List<Video> videos=new ArrayList<Video>();
		for(VideoJpo videoJpo: videoJpos) {
			videos.add(videoJpo.toModel());
		}
		
		return videos;
	}

	@Override
	public Video retrieveVideoById(String videoId) {
		// TODO Auto-generated method stub
		
		Optional<VideoJpo> videoJpo=this.videoJpaRepository.findById(videoId);
		return videoJpo.get().toModel();
	}

}
