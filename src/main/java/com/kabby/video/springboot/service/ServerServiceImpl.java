package com.kabby.video.springboot.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kabby.video.domain.repository.ServerRepositoryStore;
import com.kabby.video.domain.service.ServerLogic;

@Service
@Transactional
public class ServerServiceImpl  extends ServerLogic{

	public ServerServiceImpl(ServerRepositoryStore serverRepositoryStore) {
		super(serverRepositoryStore);
	}
	
}
