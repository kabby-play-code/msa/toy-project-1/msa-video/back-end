package com.kabby.video.springboot.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kabby.video.domain.repository.VideoRepositoryStore;
import com.kabby.video.domain.service.VideoLogic;

@Service
@Transactional
public class VideoServiceImpl extends VideoLogic {

	public VideoServiceImpl(VideoRepositoryStore videoRepositoryStore) {
		super(videoRepositoryStore);
	}
}
