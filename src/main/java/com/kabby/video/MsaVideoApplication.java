package com.kabby.video;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsaVideoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsaVideoApplication.class, args);
	}

}
